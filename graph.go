package main

import (
	"fmt"
	"time"
)

// The State list should reflect the nodes from the acceptance graphs in `doc/acceptance.md`.
// None state is used in case of errors in the graph traversal
// SchrodingerState/Quantum state is used when homer cannot decide at which state the MR is
const (
	InitialState             = GraphNode("Submitted")
	NotCompliantState        = GraphNode("NotCompliant")
	PipelineNotFinishedState = GraphNode("PipelineNotFinished")
	PipelineReadyState       = GraphNode("PipelineReady")
	ReviewableState          = GraphNode("Reviewable")
	InReviewState            = GraphNode("InReview")
	WaitingForReviewerState  = GraphNode("WaitingForReviewer")
	AcceptableState          = GraphNode("Acceptable")
	AcceptedState            = GraphNode("Accepted")
	SchrodingerState         = GraphNode("QuantumState")
	StaleState               = GraphNode("Stale")
	NoneState                = GraphNode("None")
)

// MaxTransitions represents the maximum number of transitions during graph traversal.
// This is used to avoid infinite loops
const maxTransitions = 100

// GraphNode is a very simple representation of a graph node.
type GraphNode string

// GraphEdge represents an edge/transition in the graph.
// From: edge start (the transition state where the condition will be evaluated)
// Condition: a function that will evaluate if the transition condition is fulfilled.
// To: edge end (this state will be the new current one if the Condition is fulfilled.
// ToOnFalse: if the Condition is not NOT OK and ToOnFalse is equal to NoneState,
//
//	nothing happens (the next edge is evaluated). Otherwise, the new current state
//	will be changed to ToOnFalse. This is a very simple way to implement a
//	decision-like transition
type GraphEdge struct {
	From      GraphNode
	Condition func(*MergeRequest) Check
	To        GraphNode
	ToOnFalse GraphNode
}

// SimpleEdge defines a simple non-decisional transition (ToOnFalse is set to NoneState)
func SimpleEdge(from GraphNode, condition func(*MergeRequest) Check, to GraphNode) GraphEdge {
	return GraphEdge{from, condition, to, NoneState}
}

// FindMRGraphState computes the Node where the MR should be.
// returns the final GraphNode, and the list of checks that ended to an actual state transition
// if the GraphNode is NoneState, something gone wrong.
func FindMRGraphState(mr *MergeRequest, graph []GraphEdge) (GraphNode, []Check) {
	checks := []Check{}
	state := InitialState
	iteration := 0
	for ; iteration < maxTransitions; iteration++ {
		transit := false
		for _, edge := range graph {
			if edge.From == state {
				logMRVerbose(mr.IID, "found a graph edge for state ", state)
				check := edge.Condition(mr)
				logMR(mr.IID, check.String())
				if check.Error != nil {
					checks = append(checks, check)
					return NoneState, checks
				}
				if check.Success {
					logMRVerbose(mr.IID, "Transition: ", state, " -> ", edge.To)
					checks = append(checks, check)
					state = edge.To
					transit = true
					break
				} else if edge.ToOnFalse != NoneState && state != edge.ToOnFalse {
					logMRVerbose(mr.IID, "Transition: ", state, " -> ", edge.ToOnFalse)
					checks = append(checks, check)
					state = edge.ToOnFalse
					transit = true
					break
				}
			}
		}
		if !transit {
			logMRVerbose(mr.IID, "No transition from state ", state, ", this is the final state")
			break
		}
	}
	if iteration >= maxTransitions {
		// We should never been there: this means your Graph is not deterministic.
		panic("Infinite loop in graph traversal!")
	}
	return state, checks
}

// UserAcceptanceGraph is the actual representation of the Acceptance graph for non-developer users
// Note: transitions will be evaluated in the order of definitions
func UserAcceptanceGraph() []GraphEdge {
	graph := []GraphEdge{}
	graph = append(graph, SimpleEdge(InitialState, CheckStatusNotDecidable, SchrodingerState))
	graph = append(graph, GraphEdge{InitialState, CheckMergeable, PipelineReadyState, NotCompliantState})
	graph = append(graph, SimpleEdge(PipelineReadyState, CheckPipelineIsNotFinished, PipelineNotFinishedState))
	graph = append(graph, GraphEdge{PipelineReadyState, CheckPipelines, ReviewableState, NotCompliantState})
	graph = append(graph, SimpleEdge(ReviewableState, CheckForReviewerFeedback, WaitingForReviewerState))
	graph = append(graph, SimpleEdge(ReviewableState, CheckVoteOrThread, InReviewState))
	graph = append(graph, SimpleEdge(InReviewState, CheckAllThreadResolvedAndDevScoreStrict, AcceptableState))
	graph = append(graph, SimpleEdge(InReviewState, CheckIsStaleUser, StaleState))
	graph = append(graph, SimpleEdge(AcceptableState, CheckIsNotActiveAnymoreUser, AcceptedState))
	// The other edges (Acceptable -> In Review, Accepted -> In Review, etc.) will never be trigger for now:
	// Homer will never reach the Acceptable/In Review states - votes or discussions will not meet the requirements
	return graph
}

// DeveloperAcceptanceGraph is the actual representation of the Acceptance graph for developer users
// Note: transitions will be evaluated in the order of definitions
func DeveloperAcceptanceGraph() []GraphEdge {
	graph := []GraphEdge{}
	graph = append(graph, SimpleEdge(InitialState, CheckStatusNotDecidable, SchrodingerState))
	graph = append(graph, GraphEdge{InitialState, CheckMergeable, PipelineReadyState, NotCompliantState})
	graph = append(graph, SimpleEdge(PipelineReadyState, CheckPipelineIsNotFinished, PipelineNotFinishedState))
	graph = append(graph, GraphEdge{PipelineReadyState, CheckPipelines, ReviewableState, NotCompliantState})
	graph = append(graph, SimpleEdge(ReviewableState, CheckForReviewerFeedback, WaitingForReviewerState))
	graph = append(graph, SimpleEdge(ReviewableState, CheckNoReview, AcceptedState))
	graph = append(graph, SimpleEdge(ReviewableState, CheckVoteOrThread, InReviewState))
	graph = append(graph, SimpleEdge(InReviewState, CheckAllThreadResolvedAndDevScoreNonStrict, AcceptableState))
	graph = append(graph, SimpleEdge(InReviewState, CheckIsStaleDeveloper, StaleState))
	graph = append(graph, SimpleEdge(AcceptableState, CheckIsNotActiveAnymoreDeveloper, AcceptedState))
	// The other edges (Acceptable -> In Review, Accepted -> In Review, etc.) will never be trigger for now:
	// Homer will never reach the Acceptable/In Review states - votes or discussions will not meet the requirements
	return graph
}

// CheckStatusNotDecidable checks the MR status is considered as not usable
// any status other than CanBeMerged or CannotBeMerged is considered as a quantum state.
func CheckStatusNotDecidable(mr *MergeRequest) Check {
	return Check{
		"MR status should be decidable",
		mr.MergeStatus != MergeStatusCanBeMerged && mr.MergeStatus != MergeStatusCannotBeMerged,
		fmt.Sprintf("MR internal status is '%s'", mr.MergeStatus),
		nil,
	}
}

// CheckMergeable checks the MR status is considered mergeable
func CheckMergeable(mr *MergeRequest) Check {
	return Check{
		"MR should be considered mergeable by Gitlab",
		mr.MergeStatus == MergeStatusCanBeMerged,
		fmt.Sprintf("MR internal status is '%s'", mr.MergeStatus),
		nil,
	}
}

// CheckPipelineIsNotFinished checks last pipeline of MR is still pending/not finished
func CheckPipelineIsNotFinished(mr *MergeRequest) Check {
	description := "Last pipeline is not finished yet"
	headPipeline, err := mr.GetHeadPipeline()
	if err != nil {
		return Check{description, false, "", err}
	}
	if headPipeline == nil {
		return Check{description, false, "No pipeline has been found", nil}
	}
	return Check{
		description,
		headPipeline.Status == PipelineStatusCreated ||
			headPipeline.Status == PipelineStatusWaitingForResource ||
			headPipeline.Status == PipelineStatusPreparing ||
			headPipeline.Status == PipelineStatusPending ||
			headPipeline.Status == PipelineStatusRunning ||
			headPipeline.Status == PipelineStatusManual ||
			headPipeline.Status == PipelineStatusScheduled,
		fmt.Sprintf("Last Pipeline (%d) has status %s", headPipeline.ID, headPipeline.Status),
		nil,
	}
}

// CheckPipelines checks last pipeline of MR is successful
func CheckPipelines(mr *MergeRequest) Check {
	description := "Last pipeline should be successful"
	headPipeline, err := mr.GetHeadPipeline()
	if err != nil {
		return Check{description, false, "", err}
	}
	if headPipeline == nil {
		return Check{description, false, "No pipeline has been found", nil}
	}
	return Check{
		description,
		headPipeline.Status == PipelineStatusSuccess,
		fmt.Sprintf("Last Pipeline (%d) has status %s", headPipeline.ID, headPipeline.Status),
		nil,
	}
}

// CheckVoteOrThread checks if the MR review has started, aka some votes and/or
// some threads have been opened
func CheckVoteOrThread(mr *MergeRequest) Check {
	description := "MergeRequest should have at least one external review and/or vote"
	// strictly or not, we only need the number of votes here
	_, votes, _, err := mr.DevScore(false)
	if err != nil {
		return Check{description, false, "", err}
	}
	resolvableNotes, err := mr.GetAllNotes(true)
	if err != nil {
		return Check{description, false, "", err}
	}
	noteFound := false
	for _, note := range *resolvableNotes {
		if note.Author.ID != mr.Author.ID {
			noteFound = true
			break
		}
	}
	return Check{
		description,
		votes > 0 || noteFound,
		fmt.Sprintf("Votes: %d, Note found: %t", votes, noteFound),
		nil,
	}
}

// For a Developer+ MR, the "inactivity" deadline is the later of:
// - (time of the first version of the MR or out-of-draft time) + mrInactiveHours
// - (time of the last "non rebase" version of the MR) + MAX(mrDevScoreHours, mrDevDiscussionHours)
func getDeveloperInactivityDeadline(mr *MergeRequest) (time.Time, error) {
	firstTime, err := mr.GetFirstVersionTime()
	logMRVerbose(mr.IID, "First MR Version Time:", firstTime)
	if err != nil {
		return time.Time{}, err
	}
	readyTime, err := mr.GetReadyTime()
	if err != nil {
		return time.Time{}, err
	}
	if !readyTime.IsZero() && readyTime.After(firstTime) {
		logMRVerbose(mr.IID, "Found a valid out-of-draft time, replacing first MR Version time to ", readyTime)
		firstTime = readyTime
	}
	firstDeadline := firstTime.Add(time.Duration(mrInactiveHours) * time.Hour)

	lastTime, err := mr.GetLatestVersionTime()
	logMRVerbose(mr.IID, "Last MR Version Time:", lastTime)
	if err != nil {
		return time.Time{}, err
	}
	maxHours := mrDevScoreHours
	if maxHours < mrDevDiscussionHours {
		// no max(in64) in native golang...
		maxHours = mrDevDiscussionHours
	}
	lastDeadline := lastTime.Add(time.Duration(maxHours) * time.Hour)

	if lastDeadline.After(firstDeadline) {
		return lastDeadline, nil
	}
	return firstDeadline, nil
}

// CheckNoReview checks if a MR has not received any review since a
// significant amount of time: no vote, no discussion (opened or closed)
// If the score only includes Upvotes/approvals, it should be ignored:
// upvotes/approval alone should not extend the "No Review" timer.
func CheckNoReview(mr *MergeRequest) Check {
	description := "No activity on MR (no thread, no vote) and last update is long enough"
	deadline, err := getDeveloperInactivityDeadline(mr)
	if err != nil {
		return Check{description, false, "", err}
	}
	logMRVerbose(mr.IID, "MR inactivity deadline (later of first version + inactivity threshold AND last version + discussion/score threshold) is ", deadline)
	if time.Now().Before(deadline) {
		return Check{description, false, "Deadline has not been reached yet", nil}
	}
	score, votes, _, err := mr.DevScore(false)
	if err != nil {
		return Check{description, false, "", err}
	}
	// If votes > 0 and score == votes, then the score only includes
	// upvotes/approvals. In this case, ignore the presence of votes.
	if votes > 0 && score != votes {
		return Check{description, false, fmt.Sprintf("Developer (down)votes have been found (%d)", votes), nil}
	}
	resolvableNotes, err := mr.GetAllNotes(true)
	if err != nil {
		return Check{description, false, "", err}
	}
	actualNotes := 0
	for _, note := range *resolvableNotes {
		if note.Author.ID == mr.Author.ID && note.Resolved {
			// threads the author has opened do not count, I suppose ?
			continue
		}
		actualNotes++
	}
	if actualNotes > 0 {
		return Check{description, false, fmt.Sprintf("Discussion threads have been found (%d)", actualNotes), nil}
	}
	return Check{description, true, fmt.Sprintf("No developer (down)vote or thread has been found, and the deadline has been reached (%s)", deadline), nil}
}

// CheckAllThreadResolvedAndDevScoreStrict checks if all threads have been resolved and score is strictly positive
func CheckAllThreadResolvedAndDevScoreStrict(mr *MergeRequest) Check {
	description := "All threads should be resolved, have votes and score > 0"
	check, _ := CheckAllThreadResolved(mr)
	if check.Error != nil || !check.Success {
		return check
	}
	score, votes, _, err := mr.DevScore(true)
	if err != nil {
		return Check{description, false, "", err}
	}
	if votes == 0 {
		return Check{description, false, "No Developer vote has been found", nil}
	}
	if score <= 0 {
		return Check{description, false, fmt.Sprintf("Score is not strictly positive (%d)", score), nil}
	}
	return Check{description, true, fmt.Sprintf("All threads seem resolved, developer votes (%d) found and score is strictly positive (%d)", votes, score), nil}
}

// CheckForReviewerFeedback checks if some Reviewer has been assigned to the MR.
// If so, and no feedback has been detected (thread, comment, approval, etc.), then the MR is considered as "Waiting for Reviewer Feedback".
func CheckForReviewerFeedback(mr *MergeRequest) Check {
	description := "A Reviewer has been set and no activity from the Reviewer has been detected"
	if len(mr.Reviewers) == 0 {
		return Check{description, false, "No Reviewer has been set", nil}
	}
	logMRVerbose(mr.IID, "found the following (explicit) Assigned Reviewers ", mr.Reviewers)
	// get all MR participants
	participants := map[int64]User{}
	// notes
	notes, err := mr.GetAllNotes(false)
	if err != nil {
		return Check{description, false, "", err}
	}
	for _, note := range *notes {
		if _, found := participants[note.Author.ID]; !found {
			participants[note.Author.ID] = note.Author
		}
	}
	// approvals
	approvals, err := mr.GetApprovals()
	if err != nil {
		return Check{description, false, "", err}
	}
	for _, approval := range approvals.ApprovedBy {
		if _, found := participants[approval.User.ID]; !found {
			participants[approval.User.ID] = approval.User
		}
	}
	// emojis
	emojis, err := mr.GetAwardEmojis()
	if err != nil {
		return Check{description, false, "", err}
	}
	for _, emoji := range *emojis {
		if _, found := participants[emoji.User.ID]; !found {
			participants[emoji.User.ID] = emoji.User
		}
	}
	logMRVerbose(mr.IID, "Found the following participants in MR: ", participants)
	someReviewerNeverParticipated := false
	for _, reviewer := range mr.Reviewers {
		if _, found := participants[reviewer.ID]; !found {
			someReviewerNeverParticipated = true
			break
		}
	}
	return Check{description, someReviewerNeverParticipated, "", nil}
}

// CheckAllThreadResolvedAndDevScoreNonStrict checks if all threads have been resolved and score is positive
func CheckAllThreadResolvedAndDevScoreNonStrict(mr *MergeRequest) Check {
	description := "All threads should be resolved, and score >= 0"
	check, _ := CheckAllThreadResolved(mr)
	if check.Error != nil || !check.Success {
		return check
	}
	score, _, _, err := mr.DevScore(false)
	if err != nil {
		return Check{description, false, "", err}
	}
	if score < 0 {
		return Check{description, false, fmt.Sprintf("Score is not positive (%d)", score), nil}
	}
	return Check{description, true, fmt.Sprintf("All threads seem resolved, and developers score is positive (%d)", score), nil}
}

// CheckIsNotActiveAnymoreUser checks if a User MR has been inactive for a certain time
func CheckIsNotActiveAnymoreUser(mr *MergeRequest) Check {
	return CheckIsResolvedNotActiveAnymore(mr, mrOtherDiscussionHours, mrOtherScoreHours, true)
}

// CheckIsNotActiveAnymoreDeveloper checks if a Developer MR has been inactive for a certain time
func CheckIsNotActiveAnymoreDeveloper(mr *MergeRequest) Check {
	return CheckIsResolvedNotActiveAnymore(mr, mrDevDiscussionHours, mrDevScoreHours, false)
}

// CheckIsResolvedNotActiveAnymore checks if a resolved MR has been inactiv for a certain time
func CheckIsResolvedNotActiveAnymore(mr *MergeRequest, discussionHoursThreshold int64, scoreHoursThreshold int64, strictly bool) Check {
	description := fmt.Sprintf("MergeRequest should have no activity (threads/votes) for (%dh/%dh)", discussionHoursThreshold, scoreHoursThreshold)
	check, lastResolved := CheckAllThreadResolved(mr)
	if check.Error != nil || !check.Success {
		return check
	}
	discussionThreshold := time.Duration(discussionHoursThreshold) * time.Hour
	// lastResolved can be zero time, but this has no consequence here
	if since := time.Since(lastResolved); !lastResolved.IsZero() && since < discussionThreshold {
		return Check{description, false, fmt.Sprintf("Discussions have been resolved, but too soon (%fh)", since.Hours()), nil}
	}
	_, _, lastSignChange, err := mr.DevScore(strictly)
	if err != nil {
		return Check{description, false, "", err}
	}
	voteThreshold := time.Duration(scoreHoursThreshold) * time.Hour
	// lastSignChange can be zero time, in case of a Developer MR
	if since := time.Since(lastSignChange); since < voteThreshold {
		return Check{description, false, fmt.Sprintf("Score is strictly positive, but too soon (%fh)", since.Hours()), nil}
	}
	// check if there is a ready time (aka MR has been in Draft)
	readyTime, err := mr.GetReadyTime()
	if err != nil {
		return Check{description, false, "", err}
	}
	if !readyTime.IsZero() {
		logMRVerbose(mr.IID, "Found a valid out-of-draft time: ", readyTime)
		// get max(discussionHoursThreshold, scoreHoursThreshold)
		threshold := discussionHoursThreshold
		if scoreHoursThreshold > threshold {
			threshold = scoreHoursThreshold
		}
		maxThreshold := time.Duration(threshold) * time.Hour
		if since := time.Since(readyTime); since < maxThreshold {
			return Check{description, false, fmt.Sprintf("MR is out-of-draft, but too soon (%fh)", since.Hours()), nil}
		}
	}
	// Last check if the latest version of the MR is old enough
	// This could be the case if: this is a Developer MR (strictly == False)
	//                            && Some other developer has approved the MR (devscore >0 but lastSignChange.IsZero)
	latestVersionTime, err := mr.GetLatestVersionTime()
	if err != nil {
		return Check{description, false, "", err}
	}
	if since := time.Since(latestVersionTime); since < voteThreshold {
		return Check{description, false, fmt.Sprintf("All threads resolved, developer votes > 0, BUT last MR version is not old enough (%fh)", since.Hours()), nil}
	}
	return Check{description, true, "All threads seem resolved, developer votes is strictly positive, both for enough time", nil}
}

// CheckIsStaleUser checks if an unresolved User MR has been inactive for a certain time
func CheckIsStaleUser(mr *MergeRequest) Check {
	return CheckIsStale(mr, mrStaleNumberOfDay, true)
}

// CheckIsStaleDeveloper checks if an unresolved Developer MR has been inactive for a certain time
func CheckIsStaleDeveloper(mr *MergeRequest) Check {
	return CheckIsStale(mr, mrStaleNumberOfDay, false)
}

// CheckIsStale checks if an unresolved MR has been inactive for a certain time
// This check comes after the "InReview -> Acceptable" check
func CheckIsStale(mr *MergeRequest, daysThreshold int64, strictly bool) Check {
	description := fmt.Sprintf("MergeRequest remains unresolved (threads/votes) for (%d days)", daysThreshold)
	threshold := time.Duration(daysThreshold) * 24 * time.Hour
	// Check MR latest version time
	latestVersionTime, err := mr.GetLatestVersionTime()
	if err != nil {
		return Check{description, false, "", err}
	}
	logMRVerbose(mr.IID, "Latest MR Version Time:", latestVersionTime)
	if since := time.Since(latestVersionTime); !latestVersionTime.IsZero() && since < threshold {
		return Check{description, false, fmt.Sprintf("Last MR version is not inactive for enough time (%f days)", since.Hours()/24.0), nil}
	}
	// Check MR last note time
	notes, err := mr.GetNonSystemNotes()
	if err != nil {
		return Check{description, false, "", err}
	}
	lastNoteUpdate := time.Time{}
	for _, note := range *notes {
		if note.UpdatedAt.After(lastNoteUpdate) {
			lastNoteUpdate = note.UpdatedAt
		}
	}
	if since := time.Since(lastNoteUpdate); !lastNoteUpdate.IsZero() && since < threshold {
		return Check{description, false, fmt.Sprintf("Discussions are not inactive for enough time (%f days)", since.Hours()/24.0), nil}
	}
	// Check MR last vote time
	_, _, lastSignChange, err := mr.DevScore(strictly)
	if err != nil {
		return Check{description, false, "", err}
	}
	if since := time.Since(lastSignChange); !lastSignChange.IsZero() && since < threshold {
		return Check{description, false, fmt.Sprintf("Votes are not inactive for enough time (%f days)", since.Hours()/24.0), nil}
	}
	if lastNoteUpdate.IsZero() && lastSignChange.IsZero() {
		return Check{description, false, "Cannot determine a inactivity time", nil}
	}
	return Check{description, true, "All threads and votes have been inactive for enough time", nil}
}

// CheckAllThreadResolved checks all resolvable threads in MR have been resolved
// Returns check, and last resolved time (zero if none),
// or last updated time if not all threads have been resolved
func CheckAllThreadResolved(mr *MergeRequest) (Check, time.Time) {
	description := "All Threads should be resolved"
	notes, err := mr.GetAllNotes(true)
	if err != nil {
		return Check{description, false, "", err}, time.Time{}
	}
	logMRVerbose(mr.IID, "Found ", len(*notes), " resolvable threads")
	allResolved := true
	lastResolved := time.Time{}
	reason := "No unresolved thread has been found"
	for _, note := range *notes {
		if !note.Resolved {
			allResolved = false
			reason = fmt.Sprintf("A thread is still [unresolved](#note_%d)", note.ID)
		}
		//if note.Author.ID != mr.Author.ID && note.ResolvedBy != nil && note.ResolvedBy.ID == mr.Author.ID {
		//	// NOTE: should we do something else ? A "Invalid" label on the MR ?
		//	return Check{description, false, fmt.Sprintf("A thread not started by the MR Author has been closed by the MR Author: [thread](#note_%d)", note.ID), nil}, time.Time{}
		//}
		// resolved + legit => take updatetime into account
		resolveDate := note.UpdatedAt
		if note.ResolvedAt != nil {
			resolveDate = *note.ResolvedAt
		}
		if resolveDate.After(lastResolved) {
			lastResolved = resolveDate
		}
	}
	// Now get system notes, and try to find the "resolve all" ones
	systemNotes, err := mr.GetSystemNotes()
	if err != nil {
		return Check{description, false, "", err}, time.Time{}
	}
	for _, note := range *systemNotes {
		if note.Body == SystemNoteResolveAllThread {
			logMRVerbose(mr.IID, "Found a 'resolve all threads' system note from ", note.Author.Username, " at ", note.CreatedAt)
			if note.CreatedAt.After(lastResolved) {
				lastResolved = note.CreatedAt
			}
		}
	}
	if allResolved {
		logMRVerbose(mr.IID, "No unresolved thread has been found, and last resolved time seems to be ", lastResolved)
	}
	return Check{description, allResolved, reason, nil}, lastResolved
}
